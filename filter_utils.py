import time
import book as b
from util import slow_type, fast_type, slow_type_djin

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

verbose = False

epoche = {
    "options": {
        0: b.VERGANGENHEIT, 1: b.GEGENWART, 2: b.ZUKUNFT
        },
    "frage": "Wann soll das Buch spielen"
    }

sprache = {
    "frage": "In welcher Sprache moechtest du lesen?",
    "options": {
        0: b.ENGLISH, 1: b.PORTUGUES, 2: b.DEUTSCH, 4: b.FRANCAIS
        }
    }

physical = {
    "frage": "Moechtest du das physikalische Buch haben?"
}

hero = {
    "frage": "Welche Gestalt soll die Hauptperson haben?",
    "options": {
        0: "Mensch",
        1: "Hund",
        2: "Baer",
        3: "Katze",
        4: "Kind",
        5: "Emperor of Rome",
        6: "Spezies",
        7: "Spinne",
        8: "Elephant",
        9: "Other"
    }
}

availability = {
    "frage": "Wie available soll das Buch sein?",
    "options": {
        0: b.PORTUGUES,
        1: "electronic",
        3: "borrowIt",
        2: "i'd buy it (same as 88)",
    }
}

dauer = {
    "frage": "Wie lang soll das Buch sein?",
    "options": {
        0: "episch",
        1: "lang",
        2: "mittel",
        3: "kurz"
    }
}

seeking = {
    "frage": "Wonach steht dir der Sinn?",
    "options": {
        0: "Charakterentwicklung",
        1: b.INSPIRATION,
        2: "Gewalt",
        3: "reality",
        4: "andere Welt",
        4: "schoene sprache",
        5: "lifehack",
        6: b.FOOD4THOUGHT
    }
}

def get_selectors(narrowing_factors, selection=b.books):
    # selection = b.books
    selectors = []
    for vnf in narrowing_factors:
        valid_selection = False
        options = vnf["options"]

        # select an option
        while not valid_selection:
            slow_type_djin((vnf["frage"]))
            time.sleep(0.2)
            for o in options:
                # find out whether o is still an option:
                post_selection, pre, post = filter_by_selector(options[o], selection, False)
                if post > 0:
                    fast_type(" (%s) %s" % (o, options[o]))
            print("(88) egal")
            selector = int(input("choose now: "))
            if selector not in options.keys() and selector != 88:
                fast_type("Ooopsi, that was not a valid option!! Please choose again..")
            else:
                valid_selection = True
                # print("Out of all books, your selection narrows it down to %s. Do you want to stick)

        # process option
        if selector != 88:
            fast_type("You choice: %s, \n" % (options[selector]))
            slow_type_djin(b.random_word())
            selectors.append(options[selector])
            post_selection, pre, post = filter_by_selector(options[selector], selection, False)
            selection = post_selection
        else:
            time.sleep(0.2)
            slow_type_djin("Fair enough...")

    return selectors


# ----- METHODS --------
# DEPRECATED
def filter_books(selectors, books, verbose=False):
    valid_books = books
    for tag in selectors:
        filtered_books, pre, post = filter_by_selector(tag, valid_books, False)
        if post == 0:
            print("In that order, filtering for %s would reduce your choices below 0. Therefore this filter is skipped!" % tag)
        else:
            valid_books = filtered_books
    return valid_books

def filter_by_selector(tag, books, verbose=False):
    valid_books = {}
    for title in books:
        if tag in books[title]:
            valid_books.update({title: books[title]})
    if verbose:
        print("pre- and post-selection for %s : %s -> %s" % (tag, len(books), len(valid_books)))
    return valid_books, len(books), len(valid_books)

def exclude_by_selector(tag, books, verbose=False):
    valid_books = {}
    for title in books:
        if tag not in books[title]:
            valid_books.update({title: books[title]})
    if verbose:
        print("pre- and post-selection for %s : %s -> %s" % (tag, len(books), len(valid_books)))
    return valid_books, len(books), len(valid_books)

# print(exclude_by_selector(b.SUPER, b.books))
def choose_filters():
    slow_type_djin("Wonach wuerdest du gerne filtern?")
    categories = [("Dauer des Buches", dauer), ("Kick", seeking), ("Hauptperson", hero), ("Sprache", sprache), ("Epoche", epoche), ("Availability", availability)]
    for idx, val in enumerate(categories):
        fast_type("(%s) %s " % (idx, val[0]))

    time.sleep(0.3)
    slow_type_djin("\n(Antworte zb '1 3' fuer Kategorien 1 und 3)")
    time.sleep(0.4)
    slow_type_djin("((Die Reihenfolge ist AUCH wichtig!))")
    selectors = [int(x) for x in input().split(' ')]
    print("You chose:", [categories[x][0] for x in selectors if x in range(len(categories))],".... ")
    time.sleep(0.2)
    slow_type_djin("..." + b.random_word())
    return [categories[x][1] for x in selectors if x in range(len(categories))]

