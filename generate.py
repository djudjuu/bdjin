from util import slow_type, slow_type_djin, AFFIRMATIVES, fast_type, CRYSTAL_BALL, random_word, PATTERN1, deepcopy, PATTERN2, PATTERN3, finish
from filter_utils import filter_by_selector, filter_books
import book as b
import os, time
# import pyfiglet # TODO

# ======== helpers
def reale_or_andere_welt():
    slow_type_djin("'Reale Welt' (0)..... oder .....eine 'andere Welt' (1) ?")
    a = int(input())
    while a != 0 and a != 1:
        slow_type("please type 0 or 1")
        a = int(input())
    tag = "andere Welt" if a == 1 else "reality"
    slow_type_djin(tag + ".... as you desire....")
    slow_type(PATTERN3, 10000)
    return tag

def dog_or_human():
    slow_type_djin(" Ist das ein 'Mensch' (0)..... oder .....etwas...wie...ich bin mir nicht sicher......ein 'Hund' (1)... oder soo...also rein unter uns...ich kann mir da alles einbilden in dem Nebel.. was siehst du da?")
    a = int(input())
    while a != 0 and a != 1:
        slow_type("please type 0 or 1")
        a = int(input())
    tag = "Mensch" if a == 0 else "Hund"
    slow_type_djin(tag + ".... as you desire....")
    slow_type(PATTERN2, 10000)
    return tag

# =========== Tests ===========

def ready_for_time_control():
    slow_type_djin("WHOOOOAAA... ok you think you can mess with time?")
    slow_type_djin("This might seriously hurt your brain... So you better be prepared to think differently, think hard and to bend your mind a little bit. Let's find out...")
    slow_type_djin("Are you ready?")
    answer = input()
    if answer not in AFFIRMATIVES:
        a = "Hmmm... that was not what I wanted to hear... A simple " + random.choice(AFFIRMATIVES) + " would have been enough (for example)"
        return False
    slow_type_djin("very goood....", 30)
    time_question = "Stell dir einen Zug vor, der mit einer Kerze auf dem Dach durch ein schwarzes Loch fährt, während du selbst mit einer Kerze auf dem Kopf auf einem Glockenturm auf dem Mars stehst und eine Uhr aufziehst, die genau einen Quadratkilometer groß ist, und ein Uhu, der übrigens auch eine Kerze auf dem Kopf trägt, in entgegengesetzter Richtung zum Zug und mit Lichtgeschwindigkeit durch einen Tunnel fliegt, welcher gerade von einem anderen schwarzen Loch verschluckt wird, das ebenfalls eine Kerze auf dem Kopf trägt. (Auf der Uhr könntest du übrigens nachsehen, wie spät es auf dem Mars ist, sogar im Dunkeln, denn du hast ja eine Kerze auf dem Kopf."
    slow_type_djin(time_question)
    time.sleep(2)
    slow_type_djin("Nun,... wie lange braucht man um wieder zu Ihnen zurueckzukehren, vorrausgesezt er schafft es seine Kerze an beiden Enden anzuzuenden?")
    print("(0) eine Stunde")
    print("(1) 3 Trillionen Sekunden")
    print("(2) 1-4 BVG Minuten")
    print("(3) mehr als doppelt die Haelfte so kurz wie auf dem Hinweg")
    a2 = int(input())
    while a2 not in range(4):
        slow_type_djin("whoops! please put in a number between 0 and 3")
        a2 = int(input())
    if a2 != 3:
        slow_type_djin("Not this time....")
        return False
    else:
        return True

def ready_for_freak():
    slow_type_djin("You want to defy the elements? Become your own force of nature? First we got to make sure you master your senses....")
    slow_type_djin("Let's see how good you can see....We are going to play:")
    slow_type_djin("Dog or rabbit!")
    fast_type("(close the window that will open to come back here..)")
    os.system("xdg-open grace.mp4")
    slow_type_djin("So... Dog (0) or rabbit (1) ?")
    a = int(input())
    if a != 1:
        slow_type_djin("nope!...Maybe you should choose another power!")
        return False
    slow_type_djin("Yeaaaaaah....rabbit rabbit...")

    slow_type_djin("Next, let's test your hand-eye coordination....")
    slow_type_djin("Close your eyes and type 'security vulnerability' with as little errors as possible.")
    a = input("Please close your eyes and type now (open them again to press ENTER)")
    time.sleep(0.3)
    if a == "security vulnerability":
        slow_type_djin("AMAZING! Was du erst mit dieser power anstellen koennen wirst.... ")
    else:
        slow_type_djin("yout typed " + a)
        slow_type_djin("close enough! Very good...")
    return True


def ready_for_intelligence():
    slow_type_djin("Dog or rabbit!")
    fast_type("(close the window to come back here..)")
    os.system("xdg-open grace.mp4 2>&1")
    slow_type_djin("So... Dog (0) or rabbit (1) ?")
    a = int(input())
    if a != 1:
        slow_type_djin("nope!...Maybe you should choose another power!")
        return False
    slow_type_djin("..rabbit rabbit...")

    slow_type_djin("Ok. so you have the necessary basics to take this test. But...")
    slow_type_djin("Would you do something criminal?")
    fast_type("(y) yes of course")
    fast_type("(n) no")
    a2 = input()
    if a2 == "y":
        return True
    else:
        return False

def ready_for_inception():
    quiz = [("Schnabeltier", "Taube",0), ("Erdmaennchen","Gans", 0), ("Eisbaer", "Lemur",0), ("Hund", "Maus", 0), ("Katze", "Affe", 1),("Maulwurf", "Maultier", 0)]
    slow_type_djin("Welches ist das lustigere Tier?")
    points = 0
    for op1, op2, truth in quiz:
        fast_type("%s (0) or %s (1)" % (op1, op2))
        if int(input()) == truth:
            points += 1
    time.sleep(0.4)
    slow_type_djin("You scored %s out of %s" % (points, len(quiz)))
    if points > 3:
        return True
    else:
        return False

# TODO add spinner: https://github.com/pavdmyt/yaspin
def ready_for_magic():
    slow_type_djin("Dein fliegender Teppich fliegt ziemlich schnell auf eine Backsteinwand. Was machst du?")
    a = input()
    if a in MAGIC_ANSWERS:
        print(CRYSTAL_BALL)
        return True
    else:
        slow_type_djin("hmmm. not ready yet...")


# ============== POWERS ===================
control_time = {
    "name": "control time",
    # "category": [epoch],
    "books": ["Permutation City", "4,3,2,1"],
    "sub-books": ["Thief of Time", "4,3,2,1"],
    "test": ready_for_time_control,
    "selector": "time"
}

freak = {
    "name": "Be a freak of nature",
    "description": " You are such a creation you almost break and rewrite the laws of physics....those still exist and they do also apply to you ...but when people watch you, they will wonder if there might be not just one exception ...",
    # "category": [seeking],
    "books": ["Rumo", "Rox Schulz", "Call of the Wild"], #, "The 5th Season"],
    "sub-books": ["I am Zlatan"],
    "test": ready_for_freak,
    "selector": "freak"
}

magic = {
    "name": "Magic",
    "description": "Like really. 'Boom' stuff appears. 'Boom' you dissappear!. 'Snap; you are a cat! Meow meow... magic",
    "category": ["magic"],
    "books": ["Too like the Lightning", "Bartimaeus", "Mort"],
    "test": None,
    "selector": "magic"
}

intelligence = {
    "name": "intelligence",
    "description": "That one is obvious is it not? You are crazy smart! Others dont stand a chance because you are always a step ahead...To be honest, it's kind of boring... but maybe I just say that so you dont turn smart and trick me into giving you all the powers...",
    "tags": ["intelligence"],
    "books": ["Surely you're joking, Mr Feynman", "Enders Game", "HPMOR.com"], #, "Altered Carbon"],"Artemis Fowl", 
    "sub-books": ["Forrest Gump"],
    "test": ready_for_intelligence,
    "selector": "intelligence"
}

inception = {
    "name": "inception",
    "description": "You are the master of out of the box thinking. Of taking a new perspective. Your mind will work so differently it is easy for you to outsmart others, come up with novel things, take unique approaches.... Oh yeah, and you will be really funny too.",
    "books": ["Surely you're joking, Mr Feynman", "Prinzessin Insomnia"],# "Speaker of the Dead"],
    "sub-books": ["Going Postal"],
    "test": ready_for_inception,
    "selector": "inception"
}

all_powers = [inception, intelligence, magic, freak, control_time]


def choose_power(powers):
    slow_type_djin("....Welche Superkraft haettest du gerne..?")
    for idx, power in enumerate(powers):
        slow_type("(%s) %s" % (idx, power["name"]))
    time.sleep(2)
    # slow_type_djin("You have questions...I know...Can I really grant you those powers???...")
    # time.sleep(1.5)
    # slow_type_djin("Like really, really?")
    # time.sleep(1.5)
    # slow_type_djin("OF COURSE!!! I am DJINNI!")
    # time.sleep(1)
    # slow_type_djin("You have other questions too...        \n I know.")
    # slow_type_djin("You want to know more about the powers...")
    # time.sleep(1)
    # slow_type_djin("Ok, here they are again...") 
    # time.sleep(0.5)
    # for power in powers:
    #     slow_type_djin(power["name"], 30)
    #     slow_type_djin("         -> " + power["description"])
    #     print('')
    #     time.sleep(1.5)

    # for idx, power in enumerate(powers):
        # slow_type_djin("(%s) %s" % (idx, power["name"]))
    selected_powers = []
    while len(selected_powers) < len(powers):
        slow_type_djin("...Choose wisely...")


        choice = 1000
        while choice not in range(len(powers)):
            try:
                choice = int(input())
            except:
                slow_type("Please type one of the numbers shown above")

        slow_type_djin("You chose:" + powers[choice]["name"] + ".........  ")
        selected_powers.append(choice)

        if powers[choice]["name"] == "Magic":
            slow_type_djin("HAAA!!!")
            time.sleep(0.7)
            slow_type_djin("I applaud your choice...the powers of all powers!!..... The power to generate your own power. The powerpetum mobile, so to say.....but I am sorry though...........I am not ready yet to share that power with you... Maybe one day.")
            time.sleep(0.3)
            slow_type_djin("I know, I am a Djinni...but TECHNICALLY I am not granting you a wish, so it's totally fine if I do what I want...")
            time.sleep(0.3)
            slow_type_djin("You will have to choose another power!")
            print('')
            continue

        slow_type_djin("Hmmmmm....your wish is my command....")
        time.sleep(1)
        slow_type_djin("BUT", 200)
        time.sleep(1)
        slow_type_djin("With great power comes great responsability..")
        time.sleep(0.5)
        slow_type_djin("So I will only grant you that power if you pass a test")
        time.sleep(0.5)
        slow_type_djin("MAY THE TEST BEGIN!")
        time.sleep(2.5)

        if powers[choice]["test"]():
            slow_type_djin("Well done....you have proven worthy.")
            return powers[choice]
        else:
            slow_type_djin("Looks like you are not ready yet... But you can still choose another power")
        for idx, power in enumerate(powers):
            if idx not in selected_powers:
                slow_type_djin("(%s) %s" % (idx, power["name"]))

    # out of powers failed all tests
    slow_type_djin("Looks like you failed all the tests....... maybe you do need at least some power of intelligence.")
    time.sleep(1)
    slow_type_djin("So be it, then...")
    return intelligence


def use_power(pow):
    duper_books_power = filter_books([b.DUPER, pow["selector"]], b.books, False).keys()

    slow_type_djin("Uebe deine Power aus indem du Enter drueckst und dabei laut ein Zauberwort deiner Wahl rufst!")
    input()
    slow_type(PATTERN1, 10000)
    slow_type_djin("sehr guuuut")
    slow_type_djin("Der magische Nebel hat sich schon beinahe verdichtet..... Etwas beginnt Gestalt anyzunehmen.......")

    power_name = pow["name"]

    duper_books_power = filter_books([b.DUPER, pow["selector"]], b.books, False)
    # freak needs dog vs human
    if pow["selector"] == "freak":
        duper_books_power = filter_by_selector(dog_or_human(), duper_books_power, False)[0]

    # all others just real vs. andere welt
    if len(duper_books_power) > 1:
        time.sleep(1)
        slow_type_djin("Aaaaahaaaaaahhh....neeee moment......ich sehe ....%s Formen....Du musst noch eine entscheidung treffen:..." % len(duper_books_power))
        tag = reale_or_andere_welt()
        duper_books_power = filter_by_selector(tag, duper_books_power, False)[0]


    selected_book_key = list(duper_books_power.keys())[0]
    time.sleep(2)
    slow_type_djin("Aaaaaahhh      HAA!", 30)
    slow_type_djin("glasklar: Ein Meisterwerk!!!")
    slow_type_djin("You created ....")
    slow_type(CRYSTAL_BALL, 10000)
    time.sleep(1)
    slow_type_djin("dumdum dum dum dum ...")
    time.sleep(1)
    fast_type( " ☆.。.:*・°☆.。 " + selected_book_key + " .。.:*・°☆.。.:*・°☆" )
    time.sleep(1)
    return selected_book_key


# pow = intelligence

pow = choose_power([freak, magic, inception, intelligence])
super_book = use_power(pow)

if "alreadyRead" in b.books[super_book]:
    time.sleep(2)
    slow_type_djin("hhmmmmmmmmmmmmmm.......")
    time.sleep(1)
    slow_type_djin("Meine magical djinni mind-reading powers sagen mir, dass du dieses Buch schon gelesen hast...")
    time.sleep(1.7)
    slow_type_djin("Wiederholen wir also....")
    use_power(pow)

finish()
