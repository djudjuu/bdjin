import random
# constants
NATURE = "nature"

# categories
FOOD4THOUGHT = "food_for_thought"
INSPIRATION = "inspiration"
ENGLISH = "english"
PORTUGUES = "portugues"
DEUTSCH = "deutsch"
FRANCAIS = "francais"
LANGUAGES = [ENGLISH, PORTUGUES, DEUTSCH, FRANCAIS]
VERGANGENHEIT = "Vergangenheit"
GEGENWART = "Gegenwart"
ZUKUNFT = "Zukunft"
EPOCHE = [VERGANGENHEIT, GEGENWART, ZUKUNFT]
HAUPTPERSON = ["Mensch", "Spinne", "Elephant", "Baer", "Katze", "Hund", "Kind", "Emperor of Rome", "Spezies", "Naturgewalt", "Wesen"]
LAENGE = ["episch", "lang", "mittel", "kurz", "episodic"]
SCENARIO = ["on mobile", "in public", "lost on an island", "aloud", "high", "Ferien"]
VIOLENCE = ["extremely_violent", "very_violent", "medium_violent", "not_violent"]
SUPER = "SUPER"
DUPER = "DUPER"
PHYSICAL_BOOK = "physical"

# Labels
tags = ["Charakterentwicklung", "lifehack", "reality", "schoene sprache", "andere Welt", "sport"]


books = {
    "Call of the Wild": ["reality", "english", "Vergangenheit", "Hund", "Charakterentwicklung", "kurz", SUPER, "electronic", DUPER, "freak"],
    "Rox Schulz": ["reality", "sport", "Mensch", "deutsch", "Vergangenheit", "mittel", SUPER, "borrowIt", DUPER, "freak"],
    "Rumo": ["fantasy", "very_violent", "Hund", "Wesen", "speed", "andere Welt", "mittel", SUPER, "alreadyRead", DUPER, "freak"],
    "O Alquimista": [PORTUGUES, GEGENWART, "mittel", INSPIRATION, "Mensch"],
    "4,3,2,1": ["Charakterentwicklung", ENGLISH, GEGENWART, "Mensch", "electronic", "Kind"],
    "Too like the Lightning": ["Mensch", SUPER, "schoene Sprache", "lang", ZUKUNFT, "very_violent", "english", "electronic", DUPER, "time", "magic"],
    "Der Gang vor die Hunde": [GEGENWART, VERGANGENHEIT, DEUTSCH, "schoene sprache", "Mensch", "berlin", "mittel", PHYSICAL_BOOK],
    "food anthropologist": [GEGENWART, PHYSICAL_BOOK, "lifehack", "Mensch", ENGLISH, FOOD4THOUGHT, "mittel"],
    "Hackers and painters": ["reality", GEGENWART, "lifehack", "inspiration", "english", "Mensch", "real", PHYSICAL_BOOK, "mittel"],
    "35kg d'espoir": ["kurz", "Kind", "inspiration", FRANCAIS, GEGENWART, "Charakterentwicklung", DUPER, "time"],
    "Children of time": ["Spinne", "Spezies", ZUKUNFT, ENGLISH, "mittel"],
    "Schrecksenmeister": ["Katze", PHYSICAL_BOOK, DEUTSCH, "mittel", "andere welt", PHYSICAL_BOOK],
    "Prinzessin Insomnia": [SUPER, "Kind", PHYSICAL_BOOK, DEUTSCH, "mittel", "andere Welt", "schoene sprache", DUPER, "inception"],
    "Malcolm X": ["Mensch", "reality", GEGENWART, INSPIRATION, "very_violent", "mittel", ENGLISH, "bio",PHYSICAL_BOOK, "Charakterentwicklung"],
    "Siddharta": ["Mensch", DEUTSCH, INSPIRATION, "mittel", "schoene sprache", VERGANGENHEIT, "bio", "Charakterentwicklung", "electronic"],
    "Shantaram": ["Mensch", ENGLISH, "andere Welt", "reality", GEGENWART, SUPER, "lang", "electronic"],
    "Enders Game": ["Kind", "very_violent", "mittel", "episodic", ENGLISH, ZUKUNFT, "Charakterentwicklung", "sport", "electronic"],
    "The Mountain Shadow": ["Mensch", "mittel", "sequel", GEGENWART, ENGLISH, "electronic"],
    "Endymion": [SUPER, ZUKUNFT, "lang", "Inspiration", ENGLISH, "electronic", "Mensch"],
    "I,robot": [ZUKUNFT, "kurz", "Wesen", ENGLISH, "electronic", "electronic"],
    "3 body problem": [ZUKUNFT, GEGENWART, ENGLISH, "lang", "Spezies", "Mensch", "episodic", PHYSICAL_BOOK],
    "Altered Carbon": [ZUKUNFT, ENGLISH, SUPER, "extremely_violent", "Mensch", "mittel", "episodic"],
    "HPMOR.com": [GEGENWART, ENGLISH, "Mensch", "mittel", "on mobile", "electronic", "andere Welt", "alreadyRead", DUPER, "intelligence"],
    "Wheel of Time": [VERGANGENHEIT, ENGLISH, "episch", "Mensch", "magic", "lost on an island", PHYSICAL_BOOK],
    "13 1/2 Leben des Kaptn Blaubaer": [SUPER, "Baer", DEUTSCH, "andere Welt", "schoene sprache", "mittel"],
    "Meditations": [VERGANGENHEIT, "Charakterentwicklung", ENGLISH, "Emperor of Rome", "mittel"],
    "The only harmless great thing": [ZUKUNFT, ENGLISH, "Elephant", "kurz"],
    "Schachnovelle": ["kurz", DEUTSCH, SUPER, "Mensch", GEGENWART],
    "The 5th Season": ["lang", "andere Welt", "Mensch", "Charakterentwicklung", "Naturgewalt", "Other", ENGLISH],
    "DAEMON": [ZUKUNFT, ENGLISH, "electronic", "FOOD4THOUGHT", "lang", "Mensch"], # questionable
    "Speaker of the Dead": [SUPER, ENGLISH, "mittel", "Charakterentwicklung", ZUKUNFT, "Mensch", "episodic"],
    "The Tao Te Pooh": [ENGLISH, "Baer", "Inspiration", "lifehack", "kurz", "electronic"],
    "Kitchen Confidential": [ENGLISH, "electronic", "bio", "reality", "Mensch", GEGENWART, "food"],
    "Permutation City": [ENGLISH, "short", FOOD4THOUGHT, "Mensch", "Spezies", ZUKUNFT, "hardcore"],
    "Creatures of a Day": [ENGLISH, GEGENWART, "reality", "Mensch", "kurz", FOOD4THOUGHT],
    "Tides from the new Worlds": ["electronic", ZUKUNFT, "Mensch", "andere Welt", "kurz"],
    "Unlimited Memory": ["reality", GEGENWART, "lifehack", "short", "electronic"],
    "The Road less Traveled": ["reality", GEGENWART, "lifehack", "mittel", "electronic"],
    "Surely you're joking, Mr Feynman": [SUPER, GEGENWART, INSPIRATION, "reality", "kurz", "witzig", DUPER, "intelligence", "inception"],
    "Artemis Fowl": [DEUTSCH, "intelligence", "Kind", "Mensch", "andere Welt", PHYSICAL_BOOK, GEGENWART, "mittel", "episodic"],
    "Forrest Gump": [ENGLISH, "intelligence", "Kind", "Mensch", GEGENWART, "kurz"],
    "Bartimaeus": [DEUTSCH, "Kind", "Wesen", "magic", "mittel", "episodic", "witzig"],
    "Die weisse Einsamkeit": [GEGENWART, "bio", "reality", "sport", DEUTSCH, INSPIRATION, "mittel"],
    "Mort": ["andere Welt", "magic", "Charakterentwicklung", ENGLISH, "mittel"],
    "I am Zlatan": ["electronic", "mittel", ENGLISH, "reality", "bio", "sport", "speed"],
    "Going Postal": ["andere Welt", ENGLISH, "witzig", "mittel", "episodic"],
    "Epic Measures": ["reality", ENGLISH, "bio", INSPIRATION, "mittel"],
    "The Captain Class": ["reality", "lifehack", "electonic", "mittel", ENGLISH, GEGENWART],
    "Da geht ein Mensch": ["reality", "bio", "mittel", DEUTSCH, "Mensch", INSPIRATION, "Charakterentwicklung", VERGANGENHEIT]
}

# , Mort, Zlatan, Going Postal, Music/Football/Money,
# Epic Measures, Messner, Captain Class,

# class Epoch(Enum):
#     VERGANGENHEIT = 1
#     GEGENWART = 2
#     ZUKUNFT = 3

class Book:
    def __init__(self, epoche, hp, sprache, laenge, tags, superbook=False):
        self.epoche = epoche
        self.laenge = laenge
        self.sprache= sprache
        self.hauptperson = hp
        self.tags = tags
        self.superbook = superbook


adjectives = ["brave", "iiiinteresting", "aha", "hmmm", "nice", "let's see", "who would have thought that?", "(REALLY?)", "okay", "let's go", "oh yeah", "as you wish", "como quiser"]
def random_word():
    return random.choice(adjectives)

