#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import getpass
import json
from util import djinni, WELCOMETEXT, RAIO, RAIO_CODE, RAIO_DJINNI, BOOKDJINN, slow_type, fast_type,  CRYSTAL_BALL, random_word, PATTERN1, deepcopy, PATTERN2, PATTERN3, bcolors, finish
import time,sys

yellow_raio = bcolors.WARNING + RAIO + bcolors.ENDC
blue_djinn = bcolors.OKBLUE + BOOKDJINN + bcolors.ENDC
blue_raio_djinn = bcolors.OKBLUE + RAIO_DJINNI + bcolors.ENDC

def greet():
    slow_type("Oi "+ bcolors.WARNING + RAIO_CODE + bcolors.ENDC, 50)
    slow_type("Willkommen in deinem Weihnachtsgeschenk,")
    slow_type(WELCOMETEXT)
    time.sleep(1)
    slow_type("your very own personal:      ")
    time.sleep(1)
    print(blue_djinn)

    time.sleep(3.5)
    # slow_type("Here is how it works....")
    djinni("Sei gegrüßt  " + bcolors.WARNING + RAIO_CODE + bcolors.ENDC)
    time.sleep(1.4)
    djinni("Ich kann dir auf zwei Arten helfen die magische Welt der Bücher zu erkunden...")
    time.sleep(0.4)
    djinni("Erstens, kann ich dir helfen dich zu entscheiden, nach welchem Buch dir gerade der Sinn steht.... ")
    time.sleep(0.2)
    djinni("Ich verfüge nämlich über ein phänomenales Gedächtnis und über einen noch besseren Geschmack, wenn du mir diese kleine Unbescheidenheit verzeihst...")
    time.sleep(1.2)
    djinni("AUßERDEM.... ")
    time.sleep(.8)
    djinni("Werde ich dir ab und an meine Zauberkräfte zur Verfügung stellen, mit denen du dir ein Buch erlesenster Qualität ganz nach deinem Geschmack ....ähhhm.... sagen wir mal.... erzeugen kannst.")
    djinni("Letzteres kostet mich ein beträchtliches Maß meiner Zauberkraft, deshalb wird es dir nur ab und an möglich sein.")
    time.sleep(1.2)
    djinni("Beachte, dass ich nicht in diesem ....ähem... lustigem Behältnis.......wie war noch gleich das Wort.........ah richtig....LAPTOPF..... gefangen bin....")
    djinni("Ich bin aus freien Stücken hier und kann ihn jederzeit wieder verlassen....bzw ich brauche das nicht einmal, da ich an mehreren Orten gleichzeitig sein kann...")
    time.sleep(1)
    djinni("Doch genug der Schwafelei... Ich bin ein Djinni, von sowas hast du ja wohl schonmal gehört.....")

def main():
    if len(sys.argv) > 1 and sys.argv[1] == "mana":
        a = getpass.getpass("Enter magic word: ")
        if a == "schmudo":
            print("MASTER-LEVEL unlocked! Please enter new mana-level!")
            aa = int(input())
            print("new mana-value", aa)
            data = {"mana": aa}
            with open('.mana.txt', 'w') as outfile:
                json.dump(data, outfile)
                return

    if len(sys.argv) > 1 and sys.argv[1] == "rapido":
        print(blue_djinn)
        djinni("Sei gegrüßt  " + bcolors.WARNING + RAIO_CODE + bcolors.ENDC)
        print("")

    else:
        greet()
    time.sleep(0.4)
    djinni("Ich kann 2 Dinge für dich tun:....")
    time.sleep(0.4)
    a = 1000
    while a != 0 and a != 1:
        fast_type("(0) Schlage mir ein Buch vor!")
        fast_type("(1) Generiere ein Super-BUCH!")
        try:
            a = int(input())
        except:
            fast_type("Please enter a 0 or 1")

    if a == 0:
        djinni("Sehr gern....")
        print(" ")
        print(" ")
        os.system("python3 filter.py")
    if a == 1:
        djinni("Nun dann.......")
        with open('.mana.txt') as json_file:
            data = json.load(json_file)
        if data["mana"] > 0:
            os.system("python3 generate.py")
            data["mana"] = data["mana"] - 1
            with open('dontOpen.txt', 'w') as outfile:
                json.dump(data, outfile)


        else:
            djinni("Ich habe kein Mana mehr, komm ein anderes Mal (zb in 300 Jahren) wieder oder gewinne einen DJUDJIN-POINT to recharge me....")

if __name__ == "__main__":
    main()
