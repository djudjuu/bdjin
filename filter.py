import time
import book as b
import sys,random
from util import slow_type, fast_type, slow_type_djin, finish
from filter_utils import exclude_by_selector, choose_filters, get_selectors, filter_books, filter_by_selector

# -------- ACTION ------------

MAX_RESULT = 5 # try to filter down to at least that number
all_non_duper_books = exclude_by_selector(b.DUPER, b.books)[0]

filters = choose_filters()
selectors = get_selectors(filters, all_non_duper_books)

slow_type_djin("hmmm...jhooooommmm.....")
time.sleep(1)
slow_type_djin("HAA!!!!!", 300)

final_books = filter_books(selectors, exclude_by_selector(b.DUPER, b.books)[0], )

if len(final_books.keys()) > MAX_RESULT:
    slow_type("There are still more than %s books left. Try to add more criteria")
else:
    for b in list(final_books.keys()):
        fast_type( " ☆.。.:*・°☆.。" + b + " .。.:*・°☆.。.:*・°☆" )



finish()
