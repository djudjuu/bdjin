import sys,random,time

TYPING_SPEED = 90 #wpm
def slow_type_djin(t, typing_speed=TYPING_SPEED):
    tt = bcolors.OKBLUE + t + bcolors.ENDC
    for l in tt:
        sys.stdout.write(l)
        sys.stdout.flush()
        time.sleep(random.random()*10.0/typing_speed)
    print('')

def slow_type(t, typing_speed=TYPING_SPEED):
    for l in t:
        sys.stdout.write(l)
        sys.stdout.flush()
        time.sleep(random.random()*10.0/typing_speed)
    print('')



def fast_type(t, speed=300):
    slow_type(t, speed)


def djinni(t, speed=80):
    tt = bcolors.OKBLUE + t + bcolors.ENDC
    slow_type(tt, speed)

AFFIRMATIVES = ["yes", "jup", "ja", "sim", "Yes", "Jup", "Ja", "Sim", "Of course", "Naturally", "y", "1"]


CRYSTAL_BALL = """

               *    .
        '  +   ___    @    .
            .-" __"-.   +
    *      /:.'`__`'.\       '
        . |:: .'_ `. :|   *
   @      |:: '._' : :| .
      +    \:'.__.' :/       '
            /`-...-'\  '   +
   '       /         \   .    @
     *     `-.,___,.-'

"""

PATTERN1 = """
                  ____ _x***,  ____
                j^   "wF     h#"  "q
           w^""q#_   _0Np___g#L_   jLM"9*_
          0     #NM*N0##"   N#N@**0#L    ]r
      _*^*4p__gNN"    0h_   j#F    9Np__g0M^*s_
     jF    B@^"90p___g##@^^*N#h____0@""@NF    0
     _h___gF    jNP""9NF     #@"'"N#     #___j@_
   p"'"N###Np__g0F    j#p,,gN#     Bp___d###@"'"N_
  t     @"  "NNNN#gggg#@"   5##ggggNNN#P   "L    I
   9ggg0L   _F    0@^^N#_  _j@@^NN"   `L    #p_gp"
  p"'"N##ggg#L_  _f    j@""9N     L   j#NggN##@""^_
 4     #"  "0##*NN#___a&     p___g##*NN#F  "NF    0
  ^u_gdL    JF    0#@"'"-wgr"'"N##"    #_   j#p_ar"
   p"'"0NpgNNL_  _0#L    j#     ##_   j##pggN@""N,
  t_    ##"  "0NN@"'"*gggN#Mggp^""9NNNF   N#L    I
   9u__g##_   j##L    TNF"'"N#     0##_   j##p_a*"
     qF   9ggN@"'"*gggNE     #Nggp^""9#gg@"  "b
     `L    0##_    0N@^^#pgp@^@N#L    d##L   _0
      "*wr4P"'"qggg#     ]#     0Npgg^"'"#*w*"
          0_    #NN@Ns__g0#N___w@NN#L    jL
           "*mm^B     0#"" "N#F    ]F*mmM"
                "w___y^L    _Fh___a^
                   ""   9***"   ""

"""

PATTERN2 = """
                      ar
               ,e*m_  tF  g@^Np    _
          !h_  0L_JF  0#  #p_gF  _#^
           `#g_      jF9L      _g@  ___
      g@!#   #NNw__adF  "#g__g#"N  aP""#
      #mw#   0L    #L    ,#     #  `#mw@
  mg___    _a#_    ]#    #F     BL_   ___gM*
    "9N#*^^"'"Np_   9L  JF   _g@'"'"^"##""
  _ag_  N%     "*#__d"^^"#mg#""     j@" ,pmq_
 dF  B   TL____  _#        0L_______#   0_ j#
  9**"  a@"'""^9##"         B@""'" 9M_  "'"
  ___gg@"       _2F        _Bp_       "#Mgg__
 ^"'"''"@*___g#@F""#___ __jF""9##g__*@""    "
   __a_   3#"     g@F"#MF"Nh_     3@   apmg
   0C jF  jF    _g@   j#   "#L     #   #__2F
    "'"  g#_ammg#"    ]#     0Ne*wgj#_ "'""
       j#@"     5h   _j#__  j@     ""NM_
      *"  ,p**g  0Lg@"  "9M_0   #F9M   "^
          9L__#  JN" __a_  N#  `h__#
           "'"   #F  #" j#  9L
                J#   `**@"   @

"""

PATTERN3 = """
    ^+xw*"'"^q_  0 p" F  F _F  p^^"___jM   j  F              F
      _,,__   q x" [  F J_ J  P  w"'""_  _,"  9  _m^`"*____x"    _____
 v,,_w"   "M_ @ `, ",_!u_9__L F #  r^""^^"    f j"      "      _*"   "6_
     _,,__  B 9_ "v,_Zp*"'"'"^@u# P _m^"^u,a*"  j   ____       9       ""
   _F    `4 A_ "*-ap"            ^Lj" _smu,    _* ,-"   9_   _wf
 ^^"__,,_ jL  -- m<                5j! ____*-*^   &       "'""     ___
   p"    9p`^u,av'   maaaagic       `,*"'""q_   _x" _aa,_        p^" ""u
 ,r  _____!L___,M     maaaaaaaagic   Lsr--x_"^^`" qP     9      J      `M
   y^    "_    _J      maaaagic      L_,,_ ?_    _#       ^v- -^"
  _F  __,_`^---"jr                  j___ ""y""^^""_,-r-u_
 r^ j!    ?s_, *"jp                g"'""^q_b_    _F     "p      j^^""-w
    L  _,wma_  _x"jN__          __d"'"^c  F  "-^""  _    "c____j'      L
   j" J"    "'"  _F 99Nu______g**L_""s  4 A,    _P"'"^q_    ""         "-
 m^  j_  _-^""mw^" _' # 9"N""L ^, "s  b #   "--^"      0
      @ j"   _v-wa+" ," j  #  p  r j qF "q_   _*-wu_   9,     y^`"^w_
   _,!  0_  f   _m-**" _F _F  L _FjP ?,    "^""    "u    "---^      j
 "'"     # J   j"   p"'"p-^ x^ p" d_   -q__a-mw_    j               `L
        V  `q  #   f   j   4   b   ^,   __      6_  ?,     _,__       "--
 *`^ww-"     F 9L_ b   1   4   `u_   "-^""*,    jh    ^-xm*"   9z
            )    0 `+a_ W__ 9,___"^^"+_     L   0                k
     _x-*v+"     9    0   "b    "_    "p   _0   `&    ___       d_


"""

BOOKDJINN = """

                                                                                                                                                        
BBBBBBBBBBBBBBBBB                                     kkkkkkkk           DDDDDDDDDDDDD          jjjj   iiii                                       iiii  
B::::::::::::::::B                                    k::::::k           D::::::::::::DDD      j::::j i::::i                                     i::::i 
B::::::BBBBBB:::::B                                   k::::::k           D:::::::::::::::DD     jjjj   iiii                                       iiii  
BB:::::B     B:::::B                                  k::::::k           DDD:::::DDDDD:::::D                                                            
  B::::B     B:::::B   ooooooooooo      ooooooooooo    k:::::k    kkkkkkk  D:::::D    D:::::D jjjjjjjiiiiiiinnnn  nnnnnnnn    nnnn  nnnnnnnn    iiiiiii 
  B::::B     B:::::B oo:::::::::::oo  oo:::::::::::oo  k:::::k   k:::::k   D:::::D     D:::::Dj:::::ji:::::in:::nn::::::::nn  n:::nn::::::::nn  i:::::i 
  B::::BBBBBB:::::B o:::::::::::::::oo:::::::::::::::o k:::::k  k:::::k    D:::::D     D:::::D j::::j i::::in::::::::::::::nn n::::::::::::::nn  i::::i 
  B:::::::::::::BB  o:::::ooooo:::::oo:::::ooooo:::::o k:::::k k:::::k     D:::::D     D:::::D j::::j i::::inn:::::::::::::::nnn:::::::::::::::n i::::i 
  B::::BBBBBB:::::B o::::o     o::::oo::::o     o::::o k::::::k:::::k      D:::::D     D:::::D j::::j i::::i  n:::::nnnn:::::n  n:::::nnnn:::::n i::::i 
  B::::B     B:::::Bo::::o     o::::oo::::o     o::::o k:::::::::::k       D:::::D     D:::::D j::::j i::::i  n::::n    n::::n  n::::n    n::::n i::::i 
  B::::B     B:::::Bo::::o     o::::oo::::o     o::::o k:::::::::::k       D:::::D     D:::::D j::::j i::::i  n::::n    n::::n  n::::n    n::::n i::::i 
  B::::B     B:::::Bo::::o     o::::oo::::o     o::::o k::::::k:::::k      D:::::D    D:::::D  j::::j i::::i  n::::n    n::::n  n::::n    n::::n i::::i 
BB:::::BBBBBB::::::Bo:::::ooooo:::::oo:::::ooooo:::::ok::::::k k:::::k   DDD:::::DDDDD:::::D   j::::ji::::::i n::::n    n::::n  n::::n    n::::ni::::::i
B:::::::::::::::::B o:::::::::::::::oo:::::::::::::::ok::::::k  k:::::k  D:::::::::::::::DD    j::::ji::::::i n::::n    n::::n  n::::n    n::::ni::::::i
B::::::::::::::::B   oo:::::::::::oo  oo:::::::::::oo k::::::k   k:::::k D::::::::::::DDD      j::::ji::::::i n::::n    n::::n  n::::n    n::::ni::::::i
BBBBBBBBBBBBBBBBB      ooooooooooo      ooooooooooo   kkkkkkkk    kkkkkkkDDDDDDDDDDDDD         j::::jiiiiiiii nnnnnn    nnnnnn  nnnnnn    nnnnnniiiiiiii
                                                                                               j::::j                                                   
                                                                                     jjjj      j::::j                                                   
                                                                                    j::::jj   j:::::j                                                   
                                                                                    j::::::jjj::::::j                                                   
                                                                                     jj::::::::::::j                                                    
                                                                                       jjj::::::jjj                                                     
                                                                                          jjjjjj                                                        

"""
class bcolors:
    # HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    # FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'


RAIO_DJINNI = """
            zeeeeeee- BBBBBBBBBBBBBBBBB                                     kkkkkkkk           DDDDDDDDDDDDD          jjjj   iiii                                       iiii  
            z$$$$$$"  B::::::::::::::::B                                    k::::::k           D::::::::::::DDD      j::::j i::::i                                     i::::i 
           d$$$$$$"   B::::::BBBBBB:::::B                                   k::::::k           D:::::::::::::::DD     jjjj   iiii                                       iiii  
          d$$$$$P     BB:::::B     B:::::B                                  k::::::k           DDD:::::DDDDD:::::D                                                            
         d$$$$$P       B::::B     B:::::B   ooooooooooo      ooooooooooo    k:::::k    kkkkkkk  D:::::D    D:::::D jjjjjjjiiiiiiinnnn  nnnnnnnn    nnnn  nnnnnnnn    iiiiiii 
        $$$$$$"        B::::B     B:::::B oo:::::::::::oo  oo:::::::::::oo  k:::::k   k:::::k   D:::::D     D:::::Dj:::::ji:::::in:::nn::::::::nn  n:::nn::::::::nn  i:::::i 
      .$$$$$$"         B::::BBBBBB:::::B o:::::::::::::::oo:::::::::::::::o k:::::k  k:::::k    D:::::D     D:::::D j::::j i::::in::::::::::::::nn n::::::::::::::nn  i::::i 
     .$$$$$$"          B:::::::::::::BB  o:::::ooooo:::::oo:::::ooooo:::::o k:::::k k:::::k     D:::::D     D:::::D j::::j i::::inn:::::::::::::::nnn:::::::::::::::n i::::i 
    4$$$$$$$$$$$$$"    B::::BBBBBB:::::B o::::o     o::::oo::::o     o::::o k::::::k:::::k      D:::::D     D:::::D j::::j i::::i  n:::::nnnn:::::n  n:::::nnnn:::::n i::::i 
   z$$$$$$$$$$$$$"     B::::B     B:::::Bo::::o     o::::oo::::o     o::::o k:::::::::::k       D:::::D     D:::::D j::::j i::::i  n::::n    n::::n  n::::n    n::::n i::::i 
   "'""'""3$$$$$"      B::::B     B:::::Bo::::o     o::::oo::::o     o::::o k:::::::::::k       D:::::D     D:::::D j::::j i::::i  n::::n    n::::n  n::::n    n::::n i::::i 
         z$$$$P        B::::B     B:::::Bo::::o     o::::oo::::o     o::::o k::::::k:::::k      D:::::D    D:::::D  j::::j i::::i  n::::n    n::::n  n::::n    n::::n i::::i 
        d$$$$"        BB:::::BBBBBB::::::Bo:::::ooooo:::::oo:::::ooooo:::::ok::::::k k:::::k   DDD:::::DDDDD:::::D   j::::ji::::::i n::::n    n::::n  n::::n    n::::ni::::::i
      .$$$$$"         B:::::::::::::::::B o:::::::::::::::oo:::::::::::::::ok::::::k  k:::::k  D:::::::::::::::DD    j::::ji::::::i n::::n    n::::n  n::::n    n::::ni::::::i
     z$$$$$"          B::::::::::::::::B   oo:::::::::::oo  oo:::::::::::oo k::::::k   k:::::k D::::::::::::DDD      j::::ji::::::i n::::n    n::::n  n::::n    n::::ni::::::i
    z$$$$P            BBBBBBBBBBBBBBBBB      ooooooooooo      ooooooooooo   kkkkkkkk    kkkkkkkDDDDDDDDDDDDD         j::::jiiiiiiii nnnnnn    nnnnnn  nnnnnn    nnnnnniiiiiiii
   d$$$$$$$$$$"                                                                                                      j::::j                                                   
  *******$$$"                                                                                              jjjj      j::::j                                                   
       .$$$"                                                                                              j::::jj   j:::::j                                                   
      .$$"                                                                                                j::::::jjj::::::j                                                   
     4$P"                                                                                                  jj::::::::::::j                                                    
    z$"                                                                                                      jjj::::::jjj                                                     
   zP                                                                                                           jjjjjj                                                        
  z"
 /


"""

RAIO1 = """
            zeeeeeee-
            z$$$$$$"
           d$$$$$$"
          d$$$$$P
         d$$$$$P
        $$$$$$"
      .$$$$$$"
     .$$$$$$"
    4$$$$$$$$$$$$$"
   z$$$$$$$$$$$$$"
   "'""'""3$$$$$"
         z$$$$P
        d$$$$"
      .$$$$$"
     z$$$$$"
    z$$$$P
   d$$$$$$$$$$"
  *******$$$"
       .$$$"
      .$$"
     4$P"
    z$"
   zP
  z"
 /

"""

RAIO = """
                                                        zeeeeeee-
                                                       z$$$$$$"
                                                      d$$$$$$"
                                                     d$$$$$P
                                                    d$$$$$P
                                                   $$$$$$"
                                                 .$$$$$$"
                                                .$$$$$$"
                                               4$$$$$$$$$$$$$"
                                              z$$$$$$$$$$$$$"
                                              "'""'""3$$$$$"
                                                    z$$$$P
                                                   d$$$$"
                                                 .$$$$$"
                                                z$$$$$"
                                               z$$$$P
                                              d$$$$$$$$$$"
                                             *******$$$"
                                                  .$$$"
                                                 .$$"
                                                4$P"
                                               z$"
                                              zP
                                             z"
                                            /
"""



MAGIC_ANSWERS = []

RAIO_CODE = u"\u26A1"


adjectives = ["brave", "iiiinteresting", "aha", "hmmm", "nice", "let's see", "who would have thought that?", "(REALLY?)", "okay", "let's go", "oh yeah", "as you wish", "como quiser"]
def random_word():
    return random.choice(adjectives)

# WARNING HACKY not tested
def deepcopy(l):
    r = []
    for i in l:
        r.append(i)
    return r

WELCOMETEXT = """ Vielleicht kommt dir diese Art des Geschenks bekannt vor..... In der
fernöstlichen Kultur gilt kopieren ja als hohes Kompliment. Siehe dieses Geschenk in
diesem Sinne. Dein Geschenk hat mir soooo viel Spaß bereitet, dass ich dass dir
dieses Gefühl gerne zurück geben wollte. Kurz, dachte ich darüber nach, mich
auf dein Gebiet zu wagen und eine interaktive Zahnprothese zu bauen,.... aber
leider bin ich dann nicht über den programmierten Entwurf der selbigen
hinausgekommen... ok quatsch (kurz) beiseite.

Ich hatte ja mein Geschenk, da ich es einfach nicht abwarten konnte, einfach
auf Nikolaus vorgezogen und nun wollte ich eigentlich nur etwas kleines
nachliefern. Aber dein Geschenk hat mich nun inspiriert folgendes zu bauen...

"""
class bcolors:
    # HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    # FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'

yellow_raio = bcolors.WARNING + RAIO + bcolors.ENDC
def finish():
    slow_type_djin("Es war mir eine Freude....bis zum naechsten Mal!!!")
    print(" ")
    slow_type_djin("[Ach ja, wenn du das naechste Mal DIREKT auf meine Dienste zugreifen willst, dann gib einfach 'python3 bdjin.py rapido' ein....]")
    time.sleep(0.4)
    print(" ")
    print(" ")
    slow_type_djin("                              ate logo!")
    fast_type(yellow_raio, 8000)

